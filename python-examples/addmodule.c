#include <Python.h>

static PyObject *add_one(PyObject *self, PyObject *args)
{
    int i;

    if (!PyArg_ParseTuple(args, "i", &i))
    {
        return NULL;
    }

    return Py_BuildValue("i", i+1);
}

static PyMethodDef AddOneMethods[] = {
    {"add_one", add_one, METH_VARARGS, "Add one to integer"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef add =
{
    PyModuleDef_HEAD_INIT,
    "add",     /* name of module */
    "",        /* module documentation, may be NULL */
    -1,        /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    AddOneMethods
};

PyMODINIT_FUNC PyInit_add(void)
{
    return PyModule_Create(&add);
}
